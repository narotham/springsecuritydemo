package com.techdirective.springsecurity.demo.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomException extends RuntimeException {

    private String message;
    private HttpStatus httpStatus;

}
