package com.techdirective.springsecurity.demo.exception;

public class UserExistsException extends RuntimeException {
    public UserExistsException() {

    }

    public UserExistsException(String login) {
        super("User with login " + login + " exists already!");
    }
}
