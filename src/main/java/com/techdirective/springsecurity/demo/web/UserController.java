package com.techdirective.springsecurity.demo.web;

import com.techdirective.springsecurity.demo.service.UserService;
import com.techdirective.springsecurity.demo.service.dto.UserDTO;
import com.techdirective.springsecurity.demo.domain.User;
import com.techdirective.springsecurity.demo.exception.UserExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

/**
 * REST controller for managing users.
 */
@RestController
@RequestMapping("/api")
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    private final PasswordEncoder passwordEncoder;

    public UserController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/users/me")
    public ResponseEntity currentUser(@AuthenticationPrincipal UserDetails userDetails){
        Map<Object, Object> model = new HashMap<>();
        model.put("username", userDetails.getUsername());
        model.put("roles", userDetails.getAuthorities()
            .stream()
            .map(a -> ((GrantedAuthority) a).getAuthority())
            .collect(toList())
        );
        return ok(model);
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) {
        if (userDTO.getId() != null) {
            throw new RuntimeException("A new user cannot already have an ID");
            // Lowercase the userDTO login before comparing with database
        } else if (userService.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()) {
            throw new UserExistsException(userDTO.getLogin());
        } else {
            User usr = new User(userDTO.getId(), userDTO.getLogin(), passwordEncoder.encode(userDTO.getPassword()),
                    userDTO.getUsername(), userDTO.getEmail().toLowerCase(), userDTO.getRoles());
            User result = userService.save(usr);
            return new ResponseEntity<>(result, null, HttpStatus.OK);
        }

    }

    @PutMapping("/users")
    public ResponseEntity<User> updateUser(@Valid @RequestBody UserDTO userDTO) {

        if (!userService.findOne(userDTO.getId()).isPresent()) {
            return createUser(userDTO);
        } else {
            User usr = new User(userDTO.getId(), userDTO.getLogin(), passwordEncoder.encode(userDTO.getPassword()),
                    userDTO.getUsername(), userDTO.getEmail(), userDTO.getRoles());
            User result = userService.save(usr);
            return new ResponseEntity<>(result, null, HttpStatus.OK);
        }

    }


    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        final List<User> users = userService.findAll();
        return new ResponseEntity<>(users, null, HttpStatus.OK);
    }

    /**
     * GET /:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login" user, or with status 404 (Not Found)
     */
    @GetMapping("/users/{login}")
    public ResponseEntity<User> getUser(@PathVariable("login") String login) {
        log.debug("REST request to get User : {}", login);
        Optional<User> user = userService.findOneByLogin(login);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not found: " + login);
        }
        return ok(user.get());

    }

    /**
     * DELETE /:login : delete the User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/users/{login}")
    public ResponseEntity deleteUser(@PathVariable("login") String login) {
        log.debug("REST request to delete User: {}", login);
        userService.findOneByLogin(login).ifPresent(user -> {
            userService.delete(user.getId());
            log.debug("Deleted User: {}", user);
        });
        return noContent().build();
    }
}
