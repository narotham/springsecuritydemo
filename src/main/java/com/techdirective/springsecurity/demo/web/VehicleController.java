package com.techdirective.springsecurity.demo.web;

import com.techdirective.springsecurity.demo.domain.Vehicle;
import com.techdirective.springsecurity.demo.service.VehicleService;
import com.techdirective.springsecurity.demo.service.dto.VehicleDTO;
import com.techdirective.springsecurity.demo.exception.VehicleNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("/api")
public class VehicleController {

    private VehicleService vehicleService;
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/vehicles")
    public ResponseEntity all() {
        return ok(vehicleService.findAll());
    }
    @PostMapping("/vehicles")
    public ResponseEntity save(@RequestBody VehicleDTO form, HttpServletRequest request) {
        Vehicle saved = vehicleService.save(Vehicle.builder().name(form.getName()).build());
        return new ResponseEntity<>(saved, null, HttpStatus.OK);
    }
    @GetMapping("/vehicles/{id}")
    public ResponseEntity get(@PathVariable("id") Long id) {
        return ok(vehicleService.findOne(id).orElseThrow(VehicleNotFoundException::new));
    }

    @PutMapping("/vehicles")
    public ResponseEntity<Vehicle> update(@RequestBody VehicleDTO vehicleDTO) {
        Vehicle existed = vehicleService.findOne(vehicleDTO.getId()).orElseThrow(VehicleNotFoundException::new);
        existed.setName(vehicleDTO.getName());
        Vehicle result = vehicleService.save(existed);
        return new ResponseEntity<>(result, null, HttpStatus.OK);
    }
    @DeleteMapping("/vehicles/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        Vehicle existed = vehicleService.findOne(id).orElseThrow(VehicleNotFoundException::new);
        vehicleService.delete(existed.getId());
        return noContent().build();
    }
}
