package com.techdirective.springsecurity.demo.service;

import com.techdirective.springsecurity.demo.domain.Vehicle;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Vehicle.
 */
public interface VehicleService {

    /**
     * Save vehicle
     * @param vehicle the entity to save
     * @return the persisted entity
     */
    Vehicle save(Vehicle vehicle);

    /**
     * Get by id
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Vehicle> findOne(Long id);

    /**
     * Delete by id
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Get all Vehicles
     * @return the list of entities
     */
    List<Vehicle> findAll();

}
