package com.techdirective.springsecurity.demo.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DTO object for storing User's credentials.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthDTO implements Serializable {

    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
