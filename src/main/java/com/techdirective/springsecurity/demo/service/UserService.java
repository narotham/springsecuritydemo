package com.techdirective.springsecurity.demo.service;

import com.techdirective.springsecurity.demo.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing User.
 */
public interface UserService {

    /**
     * Save user
     * @param user the entity to save
     * @return the persisted entity
     */
    User save(User user);

    /**
     * Get by id
     * @param id the id of the entity
     * @return the entity
     */
    Optional<User> findOne(Long id);

    /**
     * Delete by id
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Get all Users
     * @return the list of entities
     */
    List<User> findAll();

    Optional<User> findOneByLogin(String login);

    Optional<User> findByUsername(String username);

    Optional<User> findOneByEmail(String email);
}
