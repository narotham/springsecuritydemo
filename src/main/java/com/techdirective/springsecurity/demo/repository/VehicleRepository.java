package com.techdirective.springsecurity.demo.repository;

import com.techdirective.springsecurity.demo.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
}
