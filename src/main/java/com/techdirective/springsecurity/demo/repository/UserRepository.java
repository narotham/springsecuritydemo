package com.techdirective.springsecurity.demo.repository;

import com.techdirective.springsecurity.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneByEmail(String email);
}

